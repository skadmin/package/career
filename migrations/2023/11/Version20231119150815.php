<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231119150815 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE career (id INT AUTO_INCREMENT NOT NULL, career_type_id INT DEFAULT NULL, career_department_id INT DEFAULT NULL, webalize VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, perex LONGTEXT NOT NULL, content LONGTEXT NOT NULL, is_active TINYINT(1) DEFAULT 1 NOT NULL, image_preview VARCHAR(255) DEFAULT NULL, sequence INT NOT NULL, created_at DATETIME NOT NULL, INDEX IDX_B25B6C84262D631F (career_type_id), INDEX IDX_B25B6C8445488FED (career_department_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE career_department (id INT AUTO_INCREMENT NOT NULL, webalize VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, content LONGTEXT NOT NULL, sequence INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE career_type (id INT AUTO_INCREMENT NOT NULL, webalize VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, content LONGTEXT NOT NULL, sequence INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE career ADD CONSTRAINT FK_B25B6C84262D631F FOREIGN KEY (career_type_id) REFERENCES career_type (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE career ADD CONSTRAINT FK_B25B6C8445488FED FOREIGN KEY (career_department_id) REFERENCES career_department (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE career DROP FOREIGN KEY FK_B25B6C84262D631F');
        $this->addSql('ALTER TABLE career DROP FOREIGN KEY FK_B25B6C8445488FED');
        $this->addSql('DROP TABLE career');
        $this->addSql('DROP TABLE career_department');
        $this->addSql('DROP TABLE career_type');
    }
}
