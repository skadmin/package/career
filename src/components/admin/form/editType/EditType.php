<?php

declare(strict_types=1);

namespace Skadmin\Career\Components\Admin;

use App\Model\System\APackageControl;
use App\Model\System\Flash;
use Nette\ComponentModel\IContainer;
use Nette\Security\User as LoggedUser;
use Nette\Utils\ArrayHash;
use Skadmin\Role\Doctrine\Role\Privilege;
use Skadmin\Career\BaseControl;
use Skadmin\Career\Doctrine\CareerType\CareerType;
use Skadmin\Career\Doctrine\CareerType\CareerTypeFacade;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\Translator\Translator;
use SkadminUtils\FormControls\UI\Form;
use SkadminUtils\FormControls\UI\FormWithUserControl;
use WebLoader\Nette\JavaScriptLoader;
use WebLoader\Nette\LoaderFactory;

class EditType extends FormWithUserControl
{
    use APackageControl;

    private LoaderFactory   $webLoader;
    private CareerTypeFacade $facade;
    private CareerType       $careerType;

    public function __construct(?int $id, CareerTypeFacade $facade, Translator $translator, LoaderFactory $webLoader, LoggedUser $user)
    {
        parent::__construct($translator, $user);
        $this->facade    = $facade;
        $this->webLoader = $webLoader;

        $this->careerType = $this->facade->get($id);
    }

    /**
     * @return static
     */
    public function setParent(?IContainer $parent, ?string $name = null): static
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::WRITE)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    public function getTitle(): SimpleTranslation|string
    {
        if ($this->careerType->isLoaded()) {
            return new SimpleTranslation('career-type.edit.title - %s', $this->careerType->getName());
        }

        return 'career-type.edit.title';
    }

    /**
     * @return JavaScriptLoader[]
     */
    public function getJs(): array
    {
        return [$this->webLoader->createJavaScriptLoader('adminTinyMce')];
    }

    public function processOnSuccess(Form $form, ArrayHash $values): void
    {
        if ($this->careerType->isLoaded()) {
            $careerType = $this->facade->update(
                $this->careerType->getId(),
                $values->name,
                $values->content
            );
            $this->onFlashmessage('form.career-type.edit.flash.success.update', Flash::SUCCESS);
        } else {
            $careerType = $this->facade->create(
                $values->name,
                $values->content
            );
            $this->onFlashmessage('form.career-type.edit.flash.success.create', Flash::SUCCESS);
        }

        if ($form->isSubmitted()->name === 'sendBack') {
            $this->processOnBack();
        }

        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render'  => 'edit-type',
            'id'      => $careerType->getId(),
        ]);
    }

    public function processOnBack(): void
    {
        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render'  => 'overview-type',
        ]);
    }

    public function render(): void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/editType.latte');

        $template->render();
    }

    protected function createComponentForm(): Form
    {
        $form = new Form();
        $form->setTranslator($this->translator);

        // INPUT
        $form->addText('name', 'form.career-type.edit.name')
            ->setRequired('form.career-type.edit.name.req');
        $form->addTextArea('content', 'form.career-type.edit.content');

        // BUTTON
        $form->addSubmit('send', 'form.career-type.edit.send');
        $form->addSubmit('sendBack', 'form.career-type.edit.send-back');
        $form->addSubmit('back', 'form.career-type.edit.back')
            ->setValidationScope([])
            ->onClick[] = [$this, 'processOnBack'];

        // DEFAULT
        $form->setDefaults($this->getDefaults());

        // CALLBACK
        $form->onSuccess[] = [$this, 'processOnSuccess'];

        return $form;
    }

    /**
     * @return mixed[]
     */
    private function getDefaults(): array
    {
        if (! $this->careerType->isLoaded()) {
            return [];
        }

        return [
            'name'    => $this->careerType->getName(),
            'content' => $this->careerType->getContent(),
        ];
    }
}
