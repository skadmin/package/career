<?php

declare(strict_types=1);

namespace Skadmin\Career\Components\Admin;

use App\Model\System\APackageControl;
use App\Model\System\Constant;
use App\Model\System\Flash;
use Nette\ComponentModel\IContainer;
use Nette\Security\User as LoggedUser;
use Nette\Utils\ArrayHash;
use Nette\Utils\Html;
use Skadmin\Career\Doctrine\CareerDepartment\CareerDepartmentFacade;
use Skadmin\Role\Doctrine\Role\Privilege;
use Skadmin\Career\BaseControl;
use Skadmin\Career\Doctrine\Career\Career;
use Skadmin\Career\Doctrine\Career\CareerFacade;
use Skadmin\Career\Doctrine\CareerType\CareerTypeFacade;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\Translator\Translator;
use SkadminUtils\FormControls\UI\Form;
use SkadminUtils\FormControls\UI\FormWithUserControl;
use SkadminUtils\FormControls\Utils\UtilsFormControl;
use SkadminUtils\ImageStorage\ImageStorage;
use WebLoader\Nette\CssLoader;
use WebLoader\Nette\JavaScriptLoader;
use WebLoader\Nette\LoaderFactory;

class Edit extends FormWithUserControl
{
    use APackageControl;

    private LoaderFactory $webLoader;
    private CareerFacade $facade;
    private CareerTypeFacade $facadeCareerType;
    private CareerDepartmentFacade $facadeCareerDepartment;
    private Career $career;
    private ImageStorage $imageStorage;

    public function __construct(?int $id, CareerFacade $facade, CareerTypeFacade $facadeCareerType, CareerDepartmentFacade $facadeCareerDepartment, Translator $translator, LoaderFactory $webLoader, LoggedUser $user, ImageStorage $imageStorage)
    {
        parent::__construct($translator, $user);
        $this->facade = $facade;
        $this->facadeCareerType = $facadeCareerType;
        $this->facadeCareerDepartment = $facadeCareerDepartment;

        $this->webLoader = $webLoader;
        $this->imageStorage = $imageStorage;

        $this->career = $this->facade->get($id);
    }

    /**
     * @return static
     */
    public function setParent(?IContainer $parent, ?string $name = null): static
    {
        parent::setParent($parent, $name);

        if (!$this->isAllowed(BaseControl::RESOURCE, Privilege::WRITE)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    public function getTitle(): SimpleTranslation|string
    {
        if ($this->career->isLoaded()) {
            return new SimpleTranslation('career.edit.title - %s', $this->career->getFullName());
        }

        return 'career.edit.title';
    }

    /**
     * @return CssLoader[]
     */
    public function getCss(): array
    {
        return [
            $this->webLoader->createCssLoader('customFileInput'),
            $this->webLoader->createCssLoader('fancyBox'), // responsive file manager
        ];
    }

    /**
     * @return JavaScriptLoader[]
     */
    public function getJs(): array
    {
        return [
            $this->webLoader->createJavaScriptLoader('adminTinyMce'),
            $this->webLoader->createJavaScriptLoader('customFileInput'),
            $this->webLoader->createJavaScriptLoader('fancyBox'), // responsive file manager
        ];
    }

    public function processOnSuccess(Form $form, ArrayHash $values): void
    {
        // IDENTIFIER
        $identifier = UtilsFormControl::getImagePreview($values->imagePreview, BaseControl::DIR_IMAGE);

        $careerDepartmentId = null;
        if ($values->careerDepartmentId !== null) {
            $careerDepartmentId = $this->facadeCareerDepartment->get($values->careerDepartmentId);
        }
        
        if ($this->career->isLoaded()) {
            if ($identifier !== null && $this->career->getImagePreview() !== null) {
                $this->imageStorage->delete($this->career->getImagePreview());
            }

            $career = $this->facade->update(
                $this->career->getId(),
                $values->name,
                $values->perex,
                $values->content,
                $values->isActive,
                $this->facadeCareerType->get($values->careerTypeId),
                $careerDepartmentId,
                $identifier,
                $values->changeWebalize
            );
            $this->onFlashmessage('form.career.edit.flash.success.update', Flash::SUCCESS);
        } else {
            $career = $this->facade->create(
                $values->name,
                $values->perex,
                $values->content,
                $values->isActive,
                $this->facadeCareerType->get($values->careerTypeId),
                $careerDepartmentId,
                $identifier,
            );
            $this->onFlashmessage('form.career.edit.flash.success.create', Flash::SUCCESS);
        }

        if ($form->isSubmitted()->name === 'sendBack') {
            $this->processOnBack();
        }

        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render' => 'edit',
            'id' => $career->getId(),
        ]);
    }

    public function processOnBack(): void
    {
        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render' => 'overview',
        ]);
    }

    public function render(): void
    {
        $template = $this->getComponentTemplate();
        $template->imageStorage = $this->imageStorage;
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/edit.latte');

        $template->career = $this->career;
        $template->render();
    }

    protected function createComponentForm(): Form
    {
        $form = new Form();
        $form->setTranslator($this->translator);

        // DATA
        $datacareerType = $this->facadeCareerType->getPairs('id', 'name');
        $datacareerDepartment = $this->facadeCareerDepartment->getPairs('id', 'name');

        // INPUT
        $form->addText('name', 'form.career.edit.name')
            ->setRequired('form.career.edit.name.req');

        $form->addTextArea('perex', 'form.career.edit.perex');
        $form->addTextArea('content', 'form.career.edit.content');

        $form->addCheckbox('isActive', 'form.career.edit.is-active')
            ->setDefaultValue(true);

        $form->addImageWithRFM('imagePreview', 'form.career.edit.image-preview');

        // EXTEND
        if ($this->career->isLoaded()) {
            $form->addCheckbox('changeWebalize', Html::el('sup', [
                'title' => $this->translator->translate('form.career.edit.change-webalize'),
                'class' => 'far fa-fw fa-question-circle',
                'data-toggle' => 'tooltip',
                'data-placement' => 'left',
            ]))->setTranslator(null);
        }

        // CAREER RELS
        $form->addSelect('careerTypeId', 'form.career.edit.career-type-id', $datacareerType)
            ->setRequired('form.career.edit.career-type-id.req')
            ->setPrompt(Constant::PROMTP)
            ->setTranslator(null);

        $form->addSelect('careerDepartmentId', 'form.career.edit.career-department-id', $datacareerDepartment)
            ->setPrompt(Constant::PROMTP)
            ->setTranslator(null);

        // BUTTON
        $form->addSubmit('send', 'form.career.edit.send');
        $form->addSubmit('sendBack', 'form.career.edit.send-back');
        $form->addSubmit('back', 'form.career.edit.back')
            ->setValidationScope([])
            ->onClick[] = [$this, 'processOnBack'];

        // DEFAULT
        $form->setDefaults($this->getDefaults());

        // CALLBACK
        $form->onSuccess[] = [$this, 'processOnSuccess'];

        return $form;
    }

    /**
     * @return mixed[]
     */
    private function getDefaults(): array
    {
        if (!$this->career->isLoaded()) {
            return [];
        }

        return [
            'name' => $this->career->getName(),
            'perex' => $this->career->getPerex(),
            'content' => $this->career->getContent(),
            'isActive' => $this->career->isActive(),
            'careerTypeId' => $this->career->getCareerType()->getId(),
            'careerDepartmentId' => $this->career->getCareerDepartment()->getId(),
        ];
    }
}
