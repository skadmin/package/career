<?php

declare(strict_types=1);

namespace Skadmin\Career\Components\Admin;

interface IEditDepartmentFactory
{
    public function create(?int $id = null): EditDepartment;
}
