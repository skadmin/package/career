<?php

declare(strict_types=1);

namespace Skadmin\Career\Components\Admin;

use App\Model\System\APackageControl;
use App\Model\System\Flash;
use Nette\ComponentModel\IContainer;
use Nette\Security\User as LoggedUser;
use Nette\Utils\ArrayHash;
use Skadmin\Role\Doctrine\Role\Privilege;
use Skadmin\Career\BaseControl;
use Skadmin\Career\Doctrine\CareerDepartment\CareerDepartment;
use Skadmin\Career\Doctrine\CareerDepartment\CareerDepartmentFacade;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\Translator\Translator;
use SkadminUtils\FormControls\UI\Form;
use SkadminUtils\FormControls\UI\FormWithUserControl;
use WebLoader\Nette\JavaScriptLoader;
use WebLoader\Nette\LoaderFactory;

class EditDepartment extends FormWithUserControl
{
    use APackageControl;

    private LoaderFactory   $webLoader;
    private CareerDepartmentFacade $facade;
    private CareerDepartment       $careerDepartment;

    public function __construct(?int $id, CareerDepartmentFacade $facade, Translator $translator, LoaderFactory $webLoader, LoggedUser $user)
    {
        parent::__construct($translator, $user);
        $this->facade    = $facade;
        $this->webLoader = $webLoader;

        $this->careerDepartment = $this->facade->get($id);
    }

    /**
     * @return static
     */
    public function setParent(?IContainer $parent, ?string $name = null): static
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::WRITE)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    public function getTitle(): SimpleTranslation|string
    {
        if ($this->careerDepartment->isLoaded()) {
            return new SimpleTranslation('career-department.edit.title - %s', $this->careerDepartment->getName());
        }

        return 'career-department.edit.title';
    }

    /**
     * @return JavaScriptLoader[]
     */
    public function getJs(): array
    {
        return [$this->webLoader->createJavaScriptLoader('adminTinyMce')];
    }

    public function processOnSuccess(Form $form, ArrayHash $values): void
    {
        if ($this->careerDepartment->isLoaded()) {
            $careerDepartment = $this->facade->update(
                $this->careerDepartment->getId(),
                $values->name,
                $values->content
            );
            $this->onFlashmessage('form.career-department.edit.flash.success.update', Flash::SUCCESS);
        } else {
            $careerDepartment = $this->facade->create(
                $values->name,
                $values->content
            );
            $this->onFlashmessage('form.career-department.edit.flash.success.create', Flash::SUCCESS);
        }

        if ($form->isSubmitted()->name === 'sendBack') {
            $this->processOnBack();
        }

        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render'  => 'edit-department',
            'id'      => $careerDepartment->getId(),
        ]);
    }

    public function processOnBack(): void
    {
        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render'  => 'overview-department',
        ]);
    }

    public function render(): void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/editDepartment.latte');

        $template->render();
    }

    protected function createComponentForm(): Form
    {
        $form = new Form();
        $form->setTranslator($this->translator);

        // INPUT
        $form->addText('name', 'form.career-department.edit.name')
            ->setRequired('form.career-department.edit.name.req');
        $form->addTextArea('content', 'form.career-department.edit.content');

        // BUTTON
        $form->addSubmit('send', 'form.career-department.edit.send');
        $form->addSubmit('sendBack', 'form.career-department.edit.send-back');
        $form->addSubmit('back', 'form.career-department.edit.back')
            ->setValidationScope([])
            ->onClick[] = [$this, 'processOnBack'];

        // DEFAULT
        $form->setDefaults($this->getDefaults());

        // CALLBACK
        $form->onSuccess[] = [$this, 'processOnSuccess'];

        return $form;
    }

    /**
     * @return mixed[]
     */
    private function getDefaults(): array
    {
        if (! $this->careerDepartment->isLoaded()) {
            return [];
        }

        return [
            'name'    => $this->careerDepartment->getName(),
            'content' => $this->careerDepartment->getContent(),
        ];
    }
}
