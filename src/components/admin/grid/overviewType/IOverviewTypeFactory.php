<?php

declare(strict_types=1);

namespace Skadmin\Career\Components\Admin;

interface IOverviewTypeFactory
{
    public function create(): OverviewType;
}
