<?php

declare(strict_types=1);

namespace Skadmin\Career\Components\Admin;

interface IOverviewFactory
{
    public function create(): Overview;
}
