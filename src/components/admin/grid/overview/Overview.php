<?php

declare(strict_types=1);

namespace Skadmin\Career\Components\Admin;

use App\Model\Grid\Traits\IsActive;
use App\Model\System\APackageControl;
use App\Model\System\Constant;
use App\Model\System\Flash;
use App\Model\System\Utils;
use Nette\ComponentModel\IContainer;
use Nette\Security\User;
use Nette\Utils\Html;
use Skadmin\Career\Doctrine\CareerDepartment\CareerDepartmentFacade;
use Skadmin\Role\Doctrine\Role\Privilege;
use Skadmin\Career\BaseControl;
use Skadmin\Career\Doctrine\Career\Career;
use Skadmin\Career\Doctrine\Career\CareerFacade;
use Skadmin\Career\Doctrine\CareerType\CareerTypeFacade;
use Skadmin\Translator\Translator;
use SkadminUtils\GridControls\UI\GridControl;
use SkadminUtils\GridControls\UI\GridDoctrine;
use SkadminUtils\ImageStorage\ImageStorage;
use WebLoader\Nette\JavaScriptLoader;
use WebLoader\Nette\LoaderFactory;

use function implode;
use function sprintf;

class Overview extends GridControl
{
    use APackageControl;
    use IsActive;

    private CareerFacade $facade;
    private LoaderFactory $webLoader;
    private CareerTypeFacade $facadeCareerType;
    private CareerDepartmentFacade $facadeCareerDepartment;
    private ImageStorage $imageStorage;

    public function __construct(CareerFacade $facade, CareerTypeFacade $facadeCareerType, CareerDepartmentFacade $facadeCareerDepartment, Translator $translator, User $user, ImageStorage $imageStorage, LoaderFactory $webLoader)
    {
        parent::__construct($translator, $user);

        $this->facade = $facade;
        $this->facadeCareerType = $facadeCareerType;
        $this->facadeCareerDepartment = $facadeCareerDepartment;
        $this->imageStorage = $imageStorage;
        $this->webLoader = $webLoader;
    }

    /**
     * @return static
     */
    public function setParent(?IContainer $parent, ?string $name = null): static
    {
        parent::setParent($parent, $name);

        if (!$this->isAllowed(BaseControl::RESOURCE, Privilege::READ)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    public function render(): void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/overview.latte');
        $template->render();
    }

    public function getTitle(): string
    {
        return 'career.overview.title';
    }

    /**
     * @return JavaScriptLoader[]
     */
    public function getJs(): array
    {
        return [
            $this->webLoader->createJavaScriptLoader('jQueryUi'),
        ];
    }

    protected function createComponentGrid(string $name): GridDoctrine
    {
        $grid = new GridDoctrine($this->getPresenter());

        // DATA
        $dataCareerType = $this->facadeCareerType->getPairs('id', 'name');
        $dataCareerDepartment = $this->facadeCareerDepartment->getPairs('id', 'name');

        // DEFAULT
        $grid->setPrimaryKey('id');
        $grid->setDataSource($this->facade->getModel()
            ->orderBy('a.sequence', 'ASC')
            ->addOrderBy('a.name', 'ASC'));

        // COLUMNS
        $grid->addColumnText('imagePreview', '')
            ->setRenderer(function (Career $career): ?Html {
                if ($career->getImagePreview() !== null) {
                    $imageSrc = $this->imageStorage->fromIdentifier([$career->getImagePreview(), '60x60', 'exact']);

                    return Html::el('img', [
                        'src' => sprintf('/%s', $imageSrc->createLink()),
                        'style' => 'max-width: none;',
                    ]);
                }

                return null;
            })->setAlign('center');
        $grid->addColumnText('name', 'grid.career.overview.name')
            ->setRenderer(function (Career $career): Html {
                if ($this->isAllowed(BaseControl::RESOURCE, 'write')) {
                    $link = $this->getPresenter()->link('Component:default', [
                        'package' => new BaseControl(),
                        'render' => 'edit',
                        'id' => $career->getId(),
                    ]);

                    $name = Html::el('a', [
                        'href' => $link,
                        'class' => 'font-weight-bold',
                    ]);
                } else {
                    $name = new Html();
                }

                $render = new Html();
                $name->setText($career->getName());

                $render->addHtml($name);

                return $render;
            });
        $grid->addColumnText('careerType', 'grid.career.overview.career-type', 'careerType.name');
        $grid->addColumnText('careerDepartment', 'grid.career.overview.career-department', 'careerDepartment.name');
        $this->addColumnIsActive($grid, 'career.overview');

        // FILTER
        $grid->addFilterText('name', 'grid.career.overview.name', ['name', 'surname', 'nickname']);
        $grid->addFilterSelect('careerType', 'grid.career.overview.career-type', $dataCareerType, 'careerType')
            ->setPrompt(Constant::PROMTP);
        $grid->addFilterSelect('careerDepartment', 'grid.career.overview.career-department', $dataCareerDepartment, 'careerDepartment')
            ->setPrompt(Constant::PROMTP);
        $this->addFilterIsActive($grid, 'career.overview');

        // ACTION
        if ($this->isAllowed(BaseControl::RESOURCE, 'write')) {
            $grid->addAction('edit', 'grid.career.overview.action.edit', 'Component:default', ['id' => 'id'])->addParameters([
                'package' => new BaseControl(),
                'render' => 'edit',
            ])->setIcon('pencil-alt')
                ->setClass('btn btn-xs btn-default btn-primary');
        }

        // TOOLBAR
        $grid->addToolbarButton('Component:default#1', 'grid.career.overview.action.career-type', [
            'package' => new BaseControl(),
            'render' => 'overview-type',
        ])->setIcon('list')
            ->setClass('btn btn-xs btn-outline-primary');

        $grid->addToolbarButton('Component:default#2', 'grid.career.overview.action.career-department', [
            'package' => new BaseControl(),
            'render' => 'overview-department',
        ])->setIcon('list')
            ->setClass('btn btn-xs btn-outline-primary');

        if ($this->isAllowed(BaseControl::RESOURCE, 'write')) {
            $grid->addToolbarButton('Component:default', 'grid.career.overview.action.new', [
                'package' => new BaseControl(),
                'render' => 'edit',
            ])->setIcon('plus')
                ->setClass('btn btn-xs btn-default btn-primary');
        }

        $grid->addToolbarSeo(BaseControl::RESOURCE, $this->getTitle());

        // SORTING
        $grid->setSortable();
        $grid->setSortableHandler($this->link('sort!'));

        return $grid;
    }

    public function handleSort(?string $itemId, ?string $prevId, ?string $nextId): void
    {
        $this->facade->sort($itemId, $prevId, $nextId);

        $presenter = $this->getPresenterIfExists();
        if ($presenter !== null) {
            $presenter->flashMessage('grid.career.overview-type.action.flash.sort.success', Flash::SUCCESS);
        }

        $this['grid']->reload();
    }
}
