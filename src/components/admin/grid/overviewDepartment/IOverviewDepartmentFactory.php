<?php

declare(strict_types=1);

namespace Skadmin\Career\Components\Admin;

interface IOverviewDepartmentFactory
{
    public function create(): OverviewDepartment;
}
