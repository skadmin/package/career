<?php

declare(strict_types=1);

namespace Skadmin\Career;

use App\Model\System\ABaseControl;
use Nette\Utils\ArrayHash;
use Nette\Utils\Html;

class BaseControl extends ABaseControl
{
    public const RESOURCE  = 'career';
    public const DIR_IMAGE = 'career';

    public function getMenu(): ArrayHash
    {
        return ArrayHash::from([
            'control' => $this,
            'icon'    => Html::el('i', ['class' => 'fas fa-fw fa-briefcase']),
            'items'   => ['overview'],
        ]);
    }
}
