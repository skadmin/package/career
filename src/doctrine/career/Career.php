<?php

declare(strict_types=1);

namespace Skadmin\Career\Doctrine\Career;

use Doctrine\ORM\Mapping as ORM;
use Skadmin\Career\Doctrine\CareerDepartment\CareerDepartment;
use Skadmin\Career\Doctrine\CareerType\CareerType;
use SkadminUtils\DoctrineTraits\Entity;

use function sprintf;

#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class Career
{
    use Entity\Id;
    use Entity\WebalizeName;
    use Entity\Perex;
    use Entity\Content;
    use Entity\IsActive;
    use Entity\ImagePreview;
    use Entity\Sequence;
    use Entity\Created;

    #[ORM\ManyToOne(targetEntity: CareerType::class, inversedBy: 'career')]
    #[ORM\JoinColumn(onDelete: 'cascade')]
    private CareerType $careerType;

    #[ORM\ManyToOne(targetEntity: CareerDepartment::class, inversedBy: 'career')]
    #[ORM\JoinColumn(onDelete: 'cascade')]
    private ?CareerDepartment $careerDepartment;

    public function update(string $name, string $perex, string $content, bool $isActive, CareerType $careerType, ?CareerDepartment $careerDepartment, ?string $imagePreview): void
    {
        $this->name = $name;
        $this->perex = $perex;
        $this->content = $content;
        $this->isActive = $isActive;

        $this->careerType = $careerType;
        $this->careerDepartment = $careerDepartment;

        if ($imagePreview === null || $imagePreview === '') {
            return;
        }

        $this->imagePreview = $imagePreview;
    }

    public function getCareerType(): CareerType
    {
        return $this->careerType;
    }

    public function getCareerDepartment(): ?CareerDepartment
    {
        return $this->careerDepartment;
    }
}
