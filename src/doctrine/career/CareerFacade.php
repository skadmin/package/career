<?php

declare(strict_types=1);

namespace Skadmin\Career\Doctrine\Career;

use Nettrine\ORM\EntityManagerDecorator;
use Skadmin\Career\Doctrine\CareerDepartment\CareerDepartment;
use Skadmin\Career\Doctrine\CareerType\CareerType;
use SkadminUtils\DoctrineTraits\Facade;

final class CareerFacade extends Facade
{
    use Facade\Sequence;
    use Facade\Webalize;

    public function __construct(EntityManagerDecorator $em)
    {
        parent::__construct($em);
        $this->table = Career::class;
    }

    public function create(string $name, string $perex, string $content, bool $isActive, CareerType $careerType, ?CareerDepartment $careerDepartment, ?string $imagePreview): Career
    {
        return $this->update(null, $name, $perex, $content, $isActive, $careerType, $careerDepartment, $imagePreview);
    }

    public function update(?int $id, string $name, string $perex, string $content, bool $isActive, CareerType $careerType, ?CareerDepartment $careerDepartment, ?string $imagePreview, bool $changeWebalize = false): Career
    {
        $career = $this->get($id);
        $career->update($name, $perex, $content, $isActive, $careerType, $careerDepartment, $imagePreview);

        if (!$career->isLoaded()) {
            $career->setSequence($this->getValidSequence());
            $career->setWebalize($this->getValidWebalize($name));
        } elseif ($changeWebalize) {
            $career->updateWebalize($this->getValidWebalize($name));
        }

        $this->em->persist($career);
        $this->em->flush();

        return $career;
    }

    public function get(?int $id = null): Career
    {
        if ($id === null) {
            return new Career();
        }

        $career = parent::get($id);

        if ($career === null) {
            return new Career();
        }

        return $career;
    }
    /**
     * @return Career[]
     */
    public function getAll(bool $onlyActive = false): array
    {
        $criteria = [];

        if ($onlyActive) {
            $criteria['isActive'] = true;
        }

        $orderBy = ['sequence' => 'ASC'];

        return $this->em
            ->getRepository($this->table)
            ->findBy($criteria, $orderBy);
    }
}
