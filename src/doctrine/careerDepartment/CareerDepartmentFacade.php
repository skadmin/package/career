<?php

declare(strict_types=1);

namespace Skadmin\Career\Doctrine\CareerDepartment;

use Nettrine\ORM\EntityManagerDecorator;
use SkadminUtils\DoctrineTraits\Facade;

use function count;

final class CareerDepartmentFacade extends Facade
{
    use Facade\Webalize;
    use Facade\Sequence;

    public function __construct(EntityManagerDecorator $em)
    {
        parent::__construct($em);
        $this->table = CareerDepartment::class;
    }

    public function create(string $name, string $content): CareerDepartment
    {
        return $this->update(null, $name, $content);
    }

    public function update(?int $id, string $name, string $content): CareerDepartment
    {
        $careerDepartment = $this->get($id);
        $careerDepartment->update($name, $content);

        if (! $careerDepartment->isLoaded()) {
            $careerDepartment->setWebalize($this->getValidWebalize($name));
            $careerDepartment->setSequence($this->getValidSequence());
        }

        $this->em->persist($careerDepartment);
        $this->em->flush();

        return $careerDepartment;
    }

    public function get(?int $id = null): CareerDepartment
    {
        if ($id === null) {
            return new CareerDepartment();
        }

        $careerDepartment = parent::get($id);

        if ($careerDepartment === null) {
            return new CareerDepartment();
        }

        return $careerDepartment;
    }

    /**
     * @param array<int> $ids
     *
     * @return CareerDepartment[]
     */
    public function findByIds(array $ids): array
    {
        $criteria = [];
        $orderBy  = ['sequence' => 'ASC'];

        if (count($ids) > 0) {
            $criteria['id'] = $ids;
        }

        return $this->em
            ->getRepository($this->table)
            ->findBy($criteria, $orderBy);
    }

    /**
     * @return CareerDepartment[]
     */
    public function getAll(): array
    {
        $criteria = [];
        $orderBy  = ['sequence' => 'ASC'];

        return $this->em
            ->getRepository($this->table)
            ->findBy($criteria, $orderBy);
    }
}
