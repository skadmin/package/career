<?php

declare(strict_types=1);

namespace Skadmin\Career\Doctrine\CareerType;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
use Skadmin\Career\Doctrine\Career\Career;
use SkadminUtils\DoctrineTraits\Entity;

#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class CareerType
{
    use Entity\Id;
    use Entity\WebalizeName;
    use Entity\Content;
    use Entity\Sequence;

    /** @var ArrayCollection|Collection|Career[] */
    #[ORM\OneToMany(targetEntity: Career::class, mappedBy: 'careerType')]
    #[ORM\OrderBy(['sequence' => 'ASC', 'name' => 'ASC'])]
    private ArrayCollection|Collection|array $career;

    public function __construct()
    {
        $this->career = new ArrayCollection();
    }

    public function update(string $name, string $content): void
    {
        $this->name    = $name;
        $this->content = $content;
    }

    /**
     * @return ArrayCollection|Collection|Career[]
     */
    public function getcareer(bool $onlyActive = false): ArrayCollection|Collection|array
    {
        if (! $onlyActive) {
            return $this->career;
        }

        $criteria = Criteria::create()->where(Criteria::expr()->eq('isActive', true));

        return $this->career->matching($criteria);
    }
}
