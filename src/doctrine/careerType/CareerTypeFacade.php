<?php

declare(strict_types=1);

namespace Skadmin\Career\Doctrine\CareerType;

use Nettrine\ORM\EntityManagerDecorator;
use SkadminUtils\DoctrineTraits\Facade;

use function count;

final class CareerTypeFacade extends Facade
{
    use Facade\Webalize;
    use Facade\Sequence;

    public function __construct(EntityManagerDecorator $em)
    {
        parent::__construct($em);
        $this->table = CareerType::class;
    }

    public function create(string $name, string $content): CareerType
    {
        return $this->update(null, $name, $content);
    }

    public function update(?int $id, string $name, string $content): CareerType
    {
        $careerType = $this->get($id);
        $careerType->update($name, $content);

        if (! $careerType->isLoaded()) {
            $careerType->setWebalize($this->getValidWebalize($name));
            $careerType->setSequence($this->getValidSequence());
        }

        $this->em->persist($careerType);
        $this->em->flush();

        return $careerType;
    }

    public function get(?int $id = null): CareerType
    {
        if ($id === null) {
            return new CareerType();
        }

        $careerType = parent::get($id);

        if ($careerType === null) {
            return new CareerType();
        }

        return $careerType;
    }

    /**
     * @param array<int> $ids
     *
     * @return CareerType[]
     */
    public function findByIds(array $ids): array
    {
        $criteria = [];
        $orderBy  = ['sequence' => 'ASC'];

        if (count($ids) > 0) {
            $criteria['id'] = $ids;
        }

        return $this->em
            ->getRepository($this->table)
            ->findBy($criteria, $orderBy);
    }

    /**
     * @return CareerType[]
     */
    public function getAll(): array
    {
        $criteria = [];
        $orderBy  = ['sequence' => 'ASC'];

        return $this->em
            ->getRepository($this->table)
            ->findBy($criteria, $orderBy);
    }
}
